

    <section class="blog-title">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <h1>Blog</h1>
                </div>
            </div>
        </div>
    </section>
    <!-- ++++ Most Bold Title ++++ -->
    <!-- ++++ blog standard content ++++ -->
    <section class="page-section bg-white o-hidden blog-content">
        <div class="container relative">
            <div class="row">
                <!-- Content -->
              
                <div class="col-sm-8">
                    <!-- Post -->
                    <!-- Post -->
                      <?php foreach($posts as $post): ?>
                    <div class="blog-item">
                        <!-- Post Title -->
                        <h2 class="blog-item-title font-alt"><a href="<?php echo site_url('/posts/'.$post['slug']); ?>"><?php echo $post['title']; ?></a></h2>
                        <!-- Date, Categories, Author, Comments -->
                        <div class="blog-item-data"> <a href="#"><i class="icon-calendar-full"></i><?php echo $post['created_at']; ?> </a> <span class="separator">&nbsp;</span> <a href="#"><i class="icon-list4"></i> Design Category</a> <span class="separator">&nbsp;</span>
                            <br class="visible-xs">
                            <a href="#"><i class="icon-user"></i> Admin</a> <span class="separator">&nbsp;</span> <a href="#"><i class="icon-bubbles"></i> Comments (2)</a> </div>
                        <!-- Image -->
                        <div class="blog-media">
                            <a href="blog-details.html"><img src="<?php echo base_url(); ?>assets/Front/images/blog/blog-img-standard-1.jpg<?php echo $post['post_img']; ?>" alt="" /></a>
                        </div>
                        <div class="blog-item-body">
                            <p> <?php echo $post['body']; ?> </p>
                        </div>
                        <!-- Read More Link -->
                        <div class="blog-item-foot"> <a href="blog-details.html" class="medium-btn3 btn btn-nofill green-text">Read More</a> </div>
                        
                    </div>
                    <!-- End Post -->
                     <?php endforeach; ?>
                    
                    <!-- End Post -->
                    <!-- Pagination -->
                    <ul class="pagination top-margin0">
                        <li class="page-item disabled"> <a class="page-link" href="#" tabindex="-1"><span class="icon-chevron-left"></span></a> </li>
                        <li class="page-item"><a class="page-link" href="#">1</a></li>
                        <li class="page-item"><a class="page-link" href="#">2</a></li>
                        <li class="page-item"><a class="page-link" href="#">3</a></li>
                        <li class="page-item"> <a class="page-link" href="#"><span class="icon-chevron-right"></span></a> </li>
                    </ul>
                    <!-- End Pagination -->
                </div>
                <!-- End Content -->
                <!-- Sidebar -->
                <div class="col-sm-4 col-md-3 col-md-offset-1 sidebar">
                    <!-- Search Widget -->
                    <div class="widget">
                        <form class="form-inline form">
                            <div class="search-wrap">
                                <button class="search-button animate" type="submit" title="Start Search"> <i class="icon-magnifier"></i> </button>
                                <input type="text" class="form-control search-field" placeholder="Search...">
                            </div>
                        </form>
                    </div>
                    <!-- End Search Widget -->
                    <!-- Widget -->
                    <div class="widget">
                        <h5 class="widget-title font-alt">Recent Posts</h5>
                        <div class="widget-body">
                            <ul class="clearlist widget-posts">
                                <li class="clearfix">
                                    <div class="widget-posts-descr"> <a href="#" title="">How to design effective teams?</a> 31 August, 2016 </div>
                                </li>
                                <li class="clearfix">
                                    <div class="widget-posts-descr"> <a href="#" title="">Eleviate design through trainingt</a> 2 September, 2016 </div>
                                </li>
                                <li class="clearfix">
                                    <div class="widget-posts-descr"> <a href="#" title="">Design is not a department but everything</a> 6 July, 2016 </div>
                                </li>
                                <li class="clearfix">
                                    <div class="widget-posts-descr"> <a href="#" title="">How to design a perfect landing page for PPC</a> 5 May, 2016 </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <!-- End Widget -->
                    <!-- Widget -->
                    <div class="widget">
                        <h5 class="widget-title font-alt">Recent Comments</h5>
                        <div class="widget-body">
                            <ul class="clearlist widget-comments">
                                <li> <span>Tony Stark</span> <a href="#" title="">Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat...</a> </li>
                                <li> <span>Tony Stark</span> <a href="#" title="">Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat...</a> </li>
                                <li> <span>Tony Stark</span> <a href="#" title="">Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat...</a> </li>
                            </ul>
                        </div>
                    </div>
                    <!-- End Widget -->
                    <!-- Widget -->
                    <div class="widget">
                        <h5 class="widget-title font-alt">Categories</h5>
                        <div class="widget-body">
                            <ul class="clearlist widget-menu">
                                <li> <a href="#" title="">Design (4)</a> </li>
                                <li> <a href="#" title="">Development (2)</a> </li>
                                <li> <a href="#" title="">Online Marketing (10)</a> </li>
                                <li> <a href="#" title="">Mobile App (3)</a> </li>
                                <li> <a href="#" title="">Website (3)</a> </li>
                            </ul>
                        </div>
                    </div>
                    <!-- End Widget -->
                    <!-- Widget -->
                    <div class="widget">
                        <h5 class="widget-title font-alt">Tags</h5>
                        <div class="widget-body">
                            <div class="tags"> <a href="#">Design</a> <a href="#">Development</a> <a href="#">Social Media</a> <a href="#">SEO</a> <a href="#">PPC</a> <a href="#">Online Marketing</a> <a href="#">Back Office</a> </div>
                        </div>
                    </div>
                    <!-- End Widget -->
                    <!-- Widget -->
                    <div class="widget">
                        <h5 class="widget-title font-alt">Archive</h5>
                        <div class="widget-body">
                            <ul class="clearlist widget-menu">
                                <li> <a href="#" title="">May 2017</a> </li>
                                <li> <a href="#" title="">April 2017</a> </li>
                                <li> <a href="#" title="">February 2017</a> </li>
                                <li> <a href="#" title="">April 2017</a> </li>
                                <li> <a href="#" title="">January 2017</a> </li>
                            </ul>
                        </div>
                    </div>
                    <!-- End Widget -->
                </div>
                <!-- End Sidebar -->
            </div>
        </div>
    </section>
    <!-- ++++ end blog Standard content ++++ -->
    <!--footer-->