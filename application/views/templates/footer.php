 <footer id="footer">
        <!--scroll to top-->
        <a class="top-btn page-scroll" href="#top"><span class="icon-chevron-up b-clor regular-text text-center"></span></a>
        <!--end scroll to top-->
        <!--newsletter section-->
        <div class="grey-dark-bg text-center">
            <div class="container">
                <h2>Sign up for our newsletter to stay up to date with tech news!</h2>
                <div class="customise-form">
                    <form>
                        <div class="row">
                            <!--box one-->
                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                <div class="form-group customised-formgroup"> <span class="icon-user"></span>
                                    <input class="form-control" placeholder="Name">
                                </div>
                            </div>
                            <!--end box one-->
                            <!--box two-->
                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                <div class="form-group customised-formgroup"> <span class="icon-envelope"></span>
                                    <input class="form-control" placeholder="Email">
                                </div>
                            </div>
                            <!--end box two-->
                            <!--box three-->
                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                <div>
                                    <input class="btn btn-fill full-width" type="submit" value="SIGN UP FOR FREE!" />
                                </div>
                            </div>
                            <!--end box three-->
                        </div>
                    </form>
                    <p class="light-gray-font-color">Lorem ipsum dolor sit amet, consectetuer adipi scing elit, sed diam nonummy nibh euismod tincidunt ut laoreet.</p>
                </div>
            </div>
        </div>
        <!--end newsletter section-->
        <!--footer content-->
        <div class="light-ash-bg">
            <div class="container">
                <ul>
                    <li>
                        <!-- footer left content-->
                        <ul>
                            <li>
                                <a href="<?php echo base_url(); ?>"><img src="<?php echo base_url(); ?>assets/Front/images/small-logo.png" alt="logo" class="img-responsive logo"></a>
                            </li>
                            <li>
                                <p class="extra-small-text">&copy; 2017</p>
                            </li>
                            <li>
                                <p class="extra-small-text">Your Company LLC.</p>
                            </li>
                        </ul>
                        <!--end footer left content-->
                    </li>
                    <li>
                        <!--footer service list-->
                        <ul>
                            <li><a class="regular-text text-color-light">Services</a></li>
                            <li><a href="<?php echo site_url('logo-branding'); ?>" class="extra-small-text">Logo &amp; Branding</a></li>
                            <li><a href="<?php echo site_url('web-developement'); ?>" class="extra-small-text">Website Development</a></li>
                            <li><a href="<?php echo site_url('Mobile-app'); ?>" class="extra-small-text">Mobile App Development</a></li>
                            <li><a href="<?php echo site_url('seo'); ?>" class="extra-small-text">Search Engine Optimization</a></li>
                            <li><a href="<?php echo site_url('ppc'); ?>" class="extra-small-text">Pay Per Click</a></li>
                            <li><a href="<?php echo site_url('social-media'); ?>" class="extra-small-text">Social Media Marketing</a></li>
                        </ul>
                        <!--end footer service list-->
                    </li>
                    <li>
                        <!--footer Resources list-->
                        <ul>
                            <li><a class="regular-text text-color-light">Resources</a></li>
                            <li><a href="<?php echo site_url('portfilo')?>" class="extra-small-text">Portfolio</a></li>
                            <li><a href="<?php echo site_url('/posts'); ?>" class="extra-small-text">Blog</a></li>
                        </ul>
                        <!--end footer Resources list-->
                    </li>
                    <li>
                        <!--footer Support list-->
                        <ul>
                            <li><a class="regular-text text-color-light">Support</a></li>
                            <li><a href="<?php echo site_url('contact')?>" class="extra-small-text">Contact</a></li>
                            <li><a href="<?php echo site_url('privacy-policy')?>" class="extra-small-text">Privacy Policy</a></li>
                            <li><a href="<?php echo site_url('terms')?>" class="extra-small-text">Terms & Conditions</a></li>
                        </ul>
                        <!--end footer Support list-->
                    </li>
                    <li class="big-width">
                        <!--footer social media list-->
                        <ul class="list-inline">
                            <li>
                                <p class="regular-text text-color-light">Get in Touch</p>
                                <ul class="social-links">
                                    <li><a href="#"><span class="icon-facebook"></span></a></li>
                                    <li><a href="#"><span class="icon-twitter"></span></a></li>
                                    <li><a href="#"><span class="icon-google-plus"></span></a></li>
                                    <li><a href="#"><span class="icon-instagram"></span></a></li>
                                    <li><a href="#"><span class="icon-pinterest"></span></a></li>
                                    <li><a href="#"><span class="icon-linkedin"></span></a></li>
                                </ul>
                            </li>
                        </ul>
                        <!--end footer social media list-->
                    </li>
                </ul>
            </div>
        </div>
        <!--end footer content-->
    </footer>
    <!--end footer-->
    
    <!--end portfolio details modal-->
    <!-- ++++ Javascript libraries ++++ -->
    <!--js library of jQuery-->
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/Front/js/jquery.min.js"></script>
    <!--custom js-->
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/Front/js/script.js"></script>
    <!--js library of bootstrap-->
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/Front/js/bootstrap.min.js"></script>
    <!--js library for number counter-->
    <script src="js/waypoints.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/Front/js/jquery.counterup.min.js"></script>
    <!--js library for video popup-->
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/Front/js/jquery.magnific-popup.min.js"></script>
    <!-- js library for owl carousel -->
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/Front/js/owl.carousel.min.js"></script>
    <!--modernizer library-->
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/Front/js/modernizr.js"></script>
    <!--js library for category filter -->
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/Front/js/jquery.mixitup.min.js"></script>
    <!-- SLIDER REVOLUTION 4.x SCRIPTS  -->
    <script type="text/javascript" src="revolution/js/jquery.themepunch.tools.min.js"></script>
    <script type="text/javascript" src="revolution/js/jquery.themepunch.revolution.min.js"></script>
    <!-- Slider revolution extensions -->
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/Front/revolution/js/extensions/revolution.extension.slideanims.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/Front/revolution/js/extensions/revolution.extension.layeranimation.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/Front/revolution/js/extensions/revolution.extension.kenburn.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/Front/revolution/js/extensions/revolution.extension.navigation.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/Front/revolution/js/extensions/revolution.extension.parallax.min.js"></script>
    
</body>



</html>

    
    
    