!DOCTYPE html>
<html lang="zxx">



<head>
    <title>DIGITAL MARKETO</title>
    <!-- description -->
    <meta name="description" content="A great collection of creative, responsive, elegant onepage templates for different industries.">
    <!-- keywords -->
    <meta name="keywords" content="css3, html5, bootstrap">
    <meta charset="utf-8">
    <meta name="author" content="">
    <meta name="viewport" content="width=device-width,initial-scale=1.0,maximum-scale=1" />
    <!-- ++++ favicon ++++ -->
    <link rel="icon" type="image/png" sizes="32x32" href="<?php echo base_url(); ?>/assets/Front/icon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="<?php echo base_url(); ?>/assets/Front/icon/favicon-96x96.png">
    <link rel="apple-touch-icon" sizes="57x57" href="<?php echo base_url(); ?>/assets/Front/icon/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="114x114" href="<?php echo base_url(); ?>/assets/Front/icon/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="114x114" href="<?php echo base_url(); ?>/assets/Front/images/apple-touch-icon-114x114.html">
    <!--  bootstrap  -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>/assets/Front/css/bootstrap.css" />
    <!-- ++++ owl carousel --->
    <link rel="stylesheet" href="<?php echo base_url(); ?>/assets/Front/css/owl.carousel.min.css" />
    <!-- --- magnific-popup  ---->
    <link rel="stylesheet" href="<?php echo base_url(); ?>/assets/Front/css/magnific-popup.css" />
    <!-- ++++ font IcoMoon ++++ -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>/assets/Front/css/fonts.css" />
    <!-- ++++ style ++++ -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>/assets/Front/css/style.css" />
    <!-- responsive css -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>/assets/Front/css/responsive.css" />
    <!-- Slider Revolution CSS Files -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>/assets/Front/revolution/css/settings.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>/assets/Front/revolution/css/layers.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>/assets/Front/revolution/css/navigation.css">
    <!-- ++++ [if IE]>
                <script src="js/html5shiv.js"></script>
                <![endif] ++++ -->
</head>

<body>
    <!-- most top information -->
    <div class="header-wrapper">
        <header id="top">
            <div class="container">
                <div class="row">
                    <div class="col-lg-7 col-md-7 col-sm-6 col-xs-12  hidden-xs">
                        <div id="custom-search-input">
                            <div class="input-group"> <span class="input-group-btn">
                                        <button class="btn" type="button"> <span class="icon-magnifier"></span> </button>
                                </span>
                                <input type="text" class="search-query form-control" placeholder="Search" />
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-5 col-md-5 col-sm-6 col-xs-12">
                        <ul class="pull-right header-right">
                            <li> <a href="tel:012.345.1234"><span class="icon-telephone"></span><span>012.345.1234</span></a></li>
                            <li> <img src="<?php echo base_url(); ?>/assets/Front/images/separator.png" alt="separator" class="img-responsive" /> </li>
                            <li> <a href="mailto:info@company.com"><span class="icon-envelope"></span><span>info@company.com</span></a> </li>
                        </ul>
                    </div>
                </div>
            </div>
        </header>
        <!-- end most top information -->
        <!--navigation-->
        <nav id="navbar-main" class="navbar main-menu">
            <div class="container">
                <!--Brand and toggle get grouped for better mobile display-->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse-1" aria-expanded="false"> <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
                    <a class="navbar-brand" href="<?php echo site_url(); ?>"><img src="<?php echo base_url(); ?>/assets/Front/images/logo.png" alt="Brand" class="img-responsive" /></a>
                </div>
                <!--Collect the nav links, and other content for toggling-->
                <div class="collapse navbar-collapse" id="navbar-collapse-1">
                    <ul class="nav navbar-nav navbar-right">
                        <li class="dropdown"> <a href="#" data-toggle="dropdown" class="dropdown-toggle">Services</a>
                            <ul class="dropdown-menu">
                                <li><a href="logo-and-branding.html"><span class="icon-palette"></span>Logo &amp; Branding</a></li>
                                <li><a href="website-development.html" class="b-clor"><span class="icon-laptop-phone"></span>Website Development</a></li>
                                <li><a href="<?= site_url('Mobile-app'); ?>"><span class="icon-smartphone-embed"></span>Mobile App Development</a></li>
                                <li><a href="search-engine-optimization.html"><span class="icon-magnifier"></span>Search Engine Optimization</a></li>
                                <li><a href="pay-per-click.html"><span class="icon-select2"></span>Pay-Per-Click</a></li>
                                <li><a href="social-media-marketing.html"><span class="icon-share"></span>Social Media Marketing</a></li>
                            </ul>
                        </li>
                        <li><a href="<?= site_url('portfilo'); ?>">Portfolio</a></li>
                        <li><a href="<?php echo site_url('about'); ?>">About</a></li>
                        <li><a href="<?php site_url('posts'); ?>">Blog</a></li>
                        <li><a href="<?= site_url('contact'); ?>">Contact</a></li>
                        <li class="btn btn-fill" data-toggle="modal" data-target="#getAQuoteModal"><a href="#">GET A QUOTE<span class="icon-chevron-right"></span></a></li>
                    </ul>
                </div>
            </div>
        </nav>
    </div>
    