    <!--carousel-->
    <!-- SLIDER -->
    <div class="main-slider">
        <div class="content">
            <div id="rev_slider_1059_1_wrapper" class="rev_slider_wrapper fullscreen-container" data-alias="concept121" data-source="gallery">
                <!-- START REVOLUTION SLIDER 5.4.1 fullwidth mode -->
                <div id="rev_slider_1078_1" class="rev_slider fullscreenbanner" style="display:none;" data-version="5.4.1">
                    <ul>
                        <!-- SLIDE  -->
                        <li data-index="rs-2972" data-transition="zoomin" data-slotamount="7" data-hideafterloop="0" data-hideslideonmobile="off" data-easein="Power4.easeInOut" data-easeout="Power4.easeInOut" data-masterspeed="2000" data-rotate="0" data-saveperformance="off" data-title="Love it?" data-param1="" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-param10="" data-description="">
                            <!-- MAIN IMAGE -->
                            <img src="<?php echo base_url(); ?>assets/Front/images/slider-1.jpg" alt="" data-bgposition="center center" data-kenburns="on" data-duration="10000" data-ease="Linear.easeNone" data-scalestart="100" data-scaleend="120" data-rotatestart="0" data-rotateend="0" data-offsetstart="0 -500" data-offsetend="0 500" data-bgparallax="10" class="rev-slidebg" data-no-retina>
                            <!-- LAYERS -->
                            <!-- Mobile only text -->
                            <div class="visible-xs caption-mobile">
                                <div class="h1">Great websites add great values</div>
                                <div class="h2">Offer full service digital media solution to your clients.</div>
                            </div>
                            <!-- LAYER NR. 1 -->
                            <div class="tp-caption NotGeneric-Title   tp-resizeme rs-parallaxlevel-3" id="slide-3046-layer-1" data-x="left" data-hoffset="0" data-y="center" data-voffset="0" data-width="none" data-height="none" data-whitespace="nowrap" data-type="text" data-responsive_offset="on" data-frames='[{"from":"y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;","mask":"x:0px;y:[100%];","speed":2000,"to":"o:1;","delay":1500,"ease":"Power4.easeInOut"},{"delay":"wait","speed":1000,"to":"y:[100%];","mask":"x:inherit;y:inherit;","ease":"Power2.easeInOut"}]' data-textAlign="['left','left','left','left']" data-paddingtop="[0,0,0,0]" data-paddingright="[0,0,0,0]" data-paddingbottom="[60]" data-paddingleft="[50,50,50,50]" style="z-index: 9; white-space: nowrap;text-transform:left;">
                                <div class="h1">Great websites add great values</div>
                            </div>
                            <!-- LAYER NR. 2 -->
                            <div class="tp-caption NotGeneric-SubTitle tp-resizeme rs-parallaxlevel-2" id="slide-3046-layer-2" data-x="left" data-hoffset="0" data-y="center" data-voffset="110" data-width="none" data-height="none" data-whitespace="nowrap" data-type="text" data-responsive_offset="on" data-frames='[{"from":"y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;","mask":"x:0px;y:[100%];","speed":2000,"to":"o:1;","delay":1500,"ease":"Power4.easeInOut"},{"delay":"wait","speed":1000,"to":"y:[100%];","mask":"x:inherit;y:inherit;","ease":"Power2.easeInOut"}]' data-textAlign="['left','left','left','left']" data-paddingtop="[0,0,0,0]" data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[50,50,50,50]" style="z-index: 9; white-space: nowrap;text-transform:left;">
                                <div class="h2">Offer full service digital media solution to your clients.</div>
                                <br>
                                <a href="#" class="medium-btn btn btn-fill" data-toggle="modal" data-target="#getAQuoteModal">GET A QUOTE <span class="icon-chevron-right"></span></a> <a class="medium-btn2 btn btn-nofill page-scroll" href="#portfolio-home">our portfolio</a> </div>
                        </li>
                        <!-- SLIDE   -->
                        <li data-index="rs-3049" data-transition="zoomin" data-slotamount="7" data-hideafterloop="0" data-hideslideonmobile="off" data-easein="Power4.easeInOut" data-easeout="Power4.easeInOut" data-masterspeed="2000" data-rotate="0" data-saveperformance="off" data-title="Love it?" data-param1="" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-param10="" data-description="">
                            <!-- MAIN IMAGE -->
                            <img src="<?php echo base_url(); ?>assets/Front/images/slider-2.jpg" alt="" data-bgposition="center center" data-kenburns="on" data-duration="10000" data-ease="Linear.easeNone" data-scalestart="100" data-scaleend="120" data-rotatestart="0" data-rotateend="0" data-offsetstart="0 -500" data-offsetend="0 500" data-bgparallax="10" class="rev-slidebg" data-no-retina>
                            <!-- LAYERS -->
                            <!-- Mobile only text -->
                            <div class="visible-xs caption-mobile">
                                <div class="h1">Great websites add great values</div>
                                <div class="h2">Offer full service digital media solution to your clients.</div>
                            </div>
                            <!-- LAYER NR. 1 -->
                            <div class="tp-caption NotGeneric-Title   tp-resizeme rs-parallaxlevel-3" id="slide-3046-layer-14" data-x="left" data-hoffset="0" data-y="center" data-voffset="0" data-width="none" data-height="none" data-whitespace="nowrap" data-type="text" data-responsive_offset="on" data-frames='[{"from":"y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;","mask":"x:0px;y:[100%];","speed":2000,"to":"o:1;","delay":1500,"ease":"Power4.easeInOut"},{"delay":"wait","speed":1000,"to":"y:[100%];","mask":"x:inherit;y:inherit;","ease":"Power2.easeInOut"}]' data-textAlign="['left','left','left','left']" data-paddingtop="[0,0,0,0]" data-paddingright="[0,0,0,0]" data-paddingbottom="[60]" data-paddingleft="[50,50,50,50]" style="z-index: 9; white-space: nowrap;text-transform:left;">
                                <div class="h1">Great websites add great values</div>
                            </div>
                            <!-- LAYER NR. 2 -->
                            <div class="tp-caption NotGeneric-SubTitle tp-resizeme rs-parallaxlevel-2" id="slide-3046-layer-15" data-x="left" data-hoffset="0" data-y="center" data-voffset="110" data-width="none" data-height="none" data-whitespace="nowrap" data-type="text" data-responsive_offset="on" data-frames='[{"from":"y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;","mask":"x:0px;y:[100%];","speed":2000,"to":"o:1;","delay":1500,"ease":"Power4.easeInOut"},{"delay":"wait","speed":1000,"to":"y:[100%];","mask":"x:inherit;y:inherit;","ease":"Power2.easeInOut"}]' data-textAlign="['left','left','left','left']" data-paddingtop="[0,0,0,0]" data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[50,50,50,50]" style="z-index: 9; white-space: nowrap;text-transform:left;">
                                <div class="h2">Offer full service digital media solution to your clients.</div>
                                <br>
                                <a href="#" class="medium-btn btn btn-fill" data-toggle="modal" data-target="#getAQuoteModal">GET A QUOTE <span class="icon-chevron-right"></span></a> <a class="medium-btn2 btn btn-nofill page-scroll" href="#portfolio-home">our portfolio</a> </div>
                        </li>
                    </ul>
                    <div class="tp-bannertimer" style="height: 7px; background-color: rgba(255, 255, 255, 0.25);"></div>
                </div>
            </div>
            <!-- END REVOLUTION SLIDER -->
        </div>
    </div>
    <!--end carousel-->
    <!--clients section-->
    <div class="clients">
        <!-- clients item slider-->
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="">
                        <div class="owl-carousel">
                            <!--slider item-->
                            <div>
                                <a href="#"><img src="<?php echo base_url(); ?>assets/Front/images/client1.png" alt="clients" class="img-responsive center-block" /></a>
                            </div>
                            <!--end slider item-->
                            <!--slider item-->
                            <div>
                                <a href="#"><img src="<?php echo base_url(); ?>assets/Front/images/client2.png" alt="clients" class="img-responsive center-block" /></a>
                            </div>
                            <!--end slider item-->
                            <!--slider item-->
                            <div>
                                <a href="#"><img src="<?php echo base_url(); ?>assets/Front/images/client3.png" alt="clients" class="img-responsive center-block" /></a>
                            </div>
                            <!--end slider item-->
                            <!--slider item-->
                            <div>
                                <a href="#"><img src="<?php echo base_url(); ?>assets/Front/images/client4.png" alt="clients" class="img-responsive center-block" /></a>
                            </div>
                            <!--end slider item-->
                            <!--slider item-->
                            <div>
                                <a href="#"><img src="<?php echo base_url(); ?>assets/Front/images/client5.png" alt="clients" class="img-responsive center-block" /></a>
                            </div>
                            <!--end slider item-->
                            <!--slider item-->
                            <div>
                                <a href="#"><img src="<?php echo base_url(); ?>assets/Front/images/client6.png" alt="clients" class="img-responsive center-block" /></a>
                            </div>
                            <!--end slider item-->
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--end clients item slider -->
    </div>
    <!--end clients section-->
    <!--services Section-->
    <section class="bg-white o-hidden services" id="services">
        <div class="container">
            <!--section title -->
            <h2 class="b-clor">Services We Provide</h2>
            <hr class="dark-line" />
            <!--end section title -->
            <div class="row">
                <!-- features box -->
                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                    <div class="box-green-border"> <a href="logo-and-branding.html"><span class="icon-palette"></span>
                                <div>Logo &AMP; Branding</div>
                            </a>
                        <p>Our comprehensive Online Marketing strategy will boost your website and traffic hence monthly sales.</p>
                    </div>
                </div>
                <!--end features box -->
                <!-- features box -->
                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                    <div class="box-green-border"> <a href="website-development.html"><span class="icon-laptop-phone"></span>
                                <div>Website Development</div>
                            </a>
                        <p>We design professional looking yet simple websites. Our designs are search engine and user friendly.</p>
                    </div>
                </div>
                <!--end features box -->
                <!-- features box -->
                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                    <div class="box-green-border"> <a href="mobile-app-development.html"><span class="icon-smartphone-embed"></span>
                                <div>Mobile App Development</div>
                            </a>
                        <p>From simple Content Management System to complex eCommerce developer, we cover it all.</p>
                    </div>
                </div>
                <!--end features box -->
            </div>
            <div class="row margin-top-15">
                <!-- features box -->
                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                    <div class="box-green-border"> <a href="search-engine-optimization.html"><span class="icon-magnifier"></span>
                                <div>Search Engine Optimization</div>
                            </a>
                        <p>We design professional looking yet simple websites. Our designs are search engine and user friendly.</p>
                    </div>
                </div>
                <!--end features box -->
                <!-- features box -->
                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                    <div class="box-green-border"> <a href="pay-per-click.html"><span class="icon-select2"></span>
                                <div>Pay-Per-Click</div>
                            </a>
                        <p>Our comprehensive Online Marketing strategy will boost your website and traffic hence monthly sales.</p>
                    </div>
                </div>
                <!--end features box -->
                <!-- features box -->
                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                    <div class="box-green-border"> <a href="social-media-marketing.html"><span class="icon-share"></span>
                                <div>Social Media Marketing</div>
                            </a>
                        <p>Our comprehensive Online Marketing strategy will boost your website and traffic hence monthly sales.</p>
                    </div>
                </div>
                <!--end features box -->
            </div>
        </div>
    </section>
    <!--end services Section-->
    <!--portfolio section-->
    <section class="bg-white o-hidden portfolio" id="portfolio-home">
        <div class="container">
            <!--section title -->
            <h2 class="b-clor">Our Impressive Portfolio</h2>
            <hr class="dark-line" />
            <!--end section title -->
            <div class="row">
                <!--portfolio item-->
                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                    <div class="protfolio-item small-image-width"> <img src="<?php echo base_url(); ?>assets/Front/images/portfolio1.jpg" alt="Brand" class="img-responsive" />
                        <div class="por-overley">
                            <div class="text-inner">
                                <p class="extra-small-text">DESIGN</p>
                                <p class="semi-bold medium-text">Responsive website design
                                    <br/> for recent client.</p>
                                <div><a class="btn btn-nofill proDetModal">DISCOVER</a></div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--end portfolio item-->
                <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                    <div class="row">
                        <!--portfolio item-->
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                            <div class="protfolio-item small-image-width"> <img src="<?php echo base_url(); ?>assets/Front/images/portfolio2.jpg" alt="Brand" class="img-responsive" />
                                <div class="por-overley">
                                    <div class="text-inner">
                                        <p class="extra-small-text">DESIGN</p>
                                        <p class="semi-bold medium-text">Responsive website design
                                            <br/> for recent client.</p>
                                        <div><a class="btn btn-nofill proDetModal">DISCOVER</a></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--end portfolio item-->
                        <!--portfolio item-->
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                            <div class="protfolio-item small-image-width"> <img src="<?php echo base_url(); ?>assets/Front/images/portfolio3.jpg" alt="Brand" class="img-responsive" />
                                <div class="por-overley">
                                    <div class="text-inner">
                                        <p class="extra-small-text">DESIGN</p>
                                        <p class="semi-bold medium-text">Responsive website design
                                            <br/> for recent client.</p>
                                        <div><a class="btn btn-nofill proDetModal">DISCOVER</a></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--end portfolio item-->
                    </div>
                    <div class="row">
                        <!--portfolio item-->
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="protfolio-item"> <img src="<?php echo base_url(); ?>assets/Front/images/portfolio4.jpg" alt="Brand" class="img-responsive" />
                                <div class="por-overley">
                                    <div class="text-inner">
                                        <p class="extra-small-text">DESIGN</p>
                                        <p class="semi-bold medium-text">Responsive website design
                                            <br/> for recent client.</p>
                                        <div><a class="btn btn-nofill proDetModal">DISCOVER</a></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--end portfolio item-->
                    </div>
                </div>
            </div>
            <!--more portfolio-->
            <div><a href="portfolio.html" class="btn btn-fill full-width">Discover more</a></div>
            <!--end more portfolio-->
        </div>
    </section>
    <!--end portfolio section-->
    <!--case studies section-->
    <section class="case-studies-section">
        <div id="case-studies-Carousel" class="case-studies-carousel carousel slide" data-ride="carousel">
            <!--Indicators-->
            <ol class="carousel-indicators">
                <li data-target="#case-studies-Carousel" data-slide-to="0" id="slide-0" class="active"> <img src="<?php echo base_url(); ?>assets/Front/images/goggle-logo.png" class="img-responsive" alt="google logo"> </li>
                <li data-target="#case-studies-Carousel" data-slide-to="1" id="slide-1"> <img src="<?php echo base_url(); ?>assets/Front/images/tohisba-logo.png" class="img-responsive" alt="tohisba logo"> </li>
                <li data-target="#case-studies-Carousel" data-slide-to="2" id="slide-2"> <img src="<?php echo base_url(); ?>assets/Front/images/canon-logo.png" class="img-responsive" alt="canon logo"> </li>
            </ol>
            <div class="carousel-inner" role="listbox">
                <!--slider item-->
                <div class="item active" style="background: url('<?php echo base_url(); ?>assets/Front/images/case-studies-slider1.jpg')">
                    <div class="container">
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="carousel-caption">
                                    <h2>Inspiring Case Studies</h2>
                                    <p class="regular-text">“Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat.”</p>
                                    <p class="extra-small-text">President, Company Name</p>
                                    <a href="#" class="small-text semi-bold video-popup"><span class="icon-play-circle"></span><span>watch video</span></a>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--end slider item-->
                <!--slider item-->
                <div class="item" style="background: url('<?php echo base_url(); ?>assets/Front/images/case-studies-slider2.jpg')">
                    <div class="container">
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="carousel-caption">
                                    <h2>Inspiring Case Studies</h2>
                                    <p class="regular-text">“Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat.”</p>
                                    <p class="extra-small-text">President, Company Name</p>
                                    <a href="#" class="small-text semi-bold video-popup"><span class="icon-play-circle"></span><span>watch video</span></a>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--end slider item-->
                <!--slider item-->
                <div class="item next" style="background: url('<?php echo base_url(); ?>assets/Front/images/case-studies-slider3.jpg')">
                    <div class="container">
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="carousel-caption">
                                    <h2>Inspiring Case Studies</h2>
                                    <p class="regular-text">“Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat.”</p>
                                    <p class="extra-small-text">President, Company Name</p>
                                    <a href="#" class="small-text semi-bold video-popup"><span class="icon-play-circle"></span><span>watch video</span></a>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--end slider item-->
            </div>
        </div>
    </section>
    <!--end case studies section-->
    <!--Choosing Us Section-->
    <section class="bg-white o-hidden why-us common-form-section" id="why-us">
        <div class="container">
            <!--section title -->
            <h2 class="b-clor">Reasons for Choosing Us</h2>
            <hr class="dark-line" />
            <!--end section title -->
            <div class="row">
                <!--text box-->
                <div class="col-lg-7 col-md-7 col-sm-6 col-xs-12">
                    <h3 class="semi-bold">We Lead from the Front</h3>
                    <p class="regular-text">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. </p>
                    <ul>
                        <li><span class="icon-man"></span>Expert guidance to build your start-up.</li>
                        <li><span class="icon-bag-dollar"></span>Save time, resource and money!</li>
                        <li><span class="icon-golf2"></span>Create endless business possibilites!</li>
                    </ul>
                    <p class="regular-text">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna. Quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. </p>
                </div>
                <!--end text box-->
                <div class="col-lg-1 col-md-1 col-sm-1 col-xs-12"> </div>
                <!--form for free quote-->
                <div class="col-lg-4 col-md-4 col-sm-5 col-xs-12">
                    <div class="customise-form">
                        <form class="email_form" method="post">
                            <div class="form-element-wrapper">
                                <h3>Get a Free Quote</h3>
                                <div class="form-group customised-formgroup"> <span class="icon-user"></span>
                                    <input type="text" name="full_name" class="form-control" placeholder="Name">
                                </div>
                                <div class="form-group customised-formgroup"> <span class="icon-envelope"></span>
                                    <input type="email" name="email" class="form-control" placeholder="Email">
                                </div>
                                <div class="form-group customised-formgroup"> <span class="icon-telephone"></span>
                                    <input type="text" name="phone" class="form-control" placeholder="Phone">
                                </div>
                                <div class="form-group customised-formgroup"> <span class="icon-laptop"></span>
                                    <input type="text" name="website" class="form-control" placeholder="Website">
                                </div>
                                <div class="form-group customised-formgroup"> <span class="icon-bubble"></span>
                                    <textarea name="message" class="form-control" placeholder="Message"></textarea>
                                </div>
                            </div>
                            <div>
                                <button type="submit" class="btn btn-fill full-width">GET A QUOTE<span class="icon-chevron-right"></span></button>
                            </div>
                        </form>
                    </div>
                </div>
                <!--end form for free quote-->
            </div>
        </div>
    </section>
    <!--end Choosing Us section-->
    <!--counter section-->
    <div class="clearfix"></div>
    <div class="banner  o-hidden success-number">
        <div class="clearfix"></div>
        <div class="parallax-container">
            <div class="clearfix"></div>
            <section>
                <div class="clearfix"></div>
                <div class="stuff" data-type="content">
                    <div class="container">
                        <!--section title -->
                        <h2>Success In Numbers</h2>
                        <!--end section title -->
                        <div class="row counter-inner">
                            <!-- features box -->
                            <div class="col-md-4 col-sm-4 col-xs-4">
                                <div class="icon-holder pull-left"><span class="icon-users2"></span></div>
                                <div class="pull-left counter-text">
                                    <div class="counter no_count b-clor">157</div>
                                    <p class="semi-bold">Happy Clients</p>
                                </div>
                            </div>
                            <!--end features box -->
                            <!-- features box -->
                            <div class="col-md-4 col-sm-4 col-xs-4">
                                <div class="icon-holder pull-left"><span class="icon-calendar-check"></span></div>
                                <div class="pull-left counter-text">
                                    <div class="counter no_count b-clor">251</div>
                                    <p class="semi-bold">projects completed</p>
                                </div>
                            </div>
                            <!--end features box -->
                            <!-- features box -->
                            <div class="col-md-4 col-sm-4 col-xs-4">
                                <div class="icon-holder pull-left"><span class="icon-clock"></span></div>
                                <div class="pull-left counter-text">
                                    <div class="counter no_count b-clor">24,100</div>
                                    <p class="semi-bold">projects completed</p>
                                </div>
                            </div>
                            <!--end features box -->
                        </div>
                    </div>
                </div>
                <div class="clearfix"></div>
            </section>
            <div class="clearfix"></div>
        </div>
    </div>
    <!--end counter section-->
    <!--Blog Section-->
    <section class="bg-white o-hidden blogs">
        <div class="container">
            <!--section title -->
            <h2 class="b-clor">Latest Blogs</h2>
            <hr class="dark-line" />
            <!--end section title -->
            <div class="row">
                <!--blog left content-->
                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                    <div class="blog-left"> <img src="<?php echo base_url(); ?>assets/Front/images/blog.jpg" alt="blog" class="img-responsive blog-img" /> <img src="<?php echo base_url(); ?>assets/Front/images/featured.png" alt="featured" class="img-responsive featured-tag " />
                        <div class="box-green-border"> <span>2 May 2015 - Business category</span> <a href="blog-details.html">Website Template for Digital Marketing Agency</a>
                            <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt. </p>
                        </div>
                    </div>
                </div>
                <!--end blog left text-->
                <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                    <div class="row">
                        <!--blog features box-->
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                            <div class="box-green-border"> <span class="small-text">22 July, 2015 - Design Category</span> <a href="blog-details.html">For Start-up Companies</a>
                                <p>Lorem ipsum dolor sit amet, consectetuer adipi scing elit, sed diam nonummy nibh euismod tincidunt ut laoreet.</p>
                            </div>
                        </div>
                        <!--end blog features box-->
                        <!--blog features box-->
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                            <div class="box-green-border"> <span class="small-text">20 June, 2015 - Branding Category</span> <a href="blog-details.html">5 Ways of Branding</a>
                                <p>Lorem ipsum dolor sit amet, consectetuer adipi scing elit, sed diam nonummy nibh euismod tincidunt ut laoreet.</p>
                            </div>
                        </div>
                        <!--end blog features box-->
                    </div>
                    <div class="row">
                        <!--blog features box-->
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                            <div class="box-green-border"> <span>2 May, 2015 - Business Category</span> <a href="blog-details.html">Essential Tips for New Entrepreneurs</a>
                                <p>Lorem ipsum dolor sit amet, consectetuer adipi scing elit, sed diam nonummy nibh euismod tincidunt ut laoreet.</p>
                            </div>
                        </div>
                        <!--end blog features box-->
                        <!--blog features box-->
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                            <div class="box-green-border"> <span>3 April, 2015 - UX Category</span> <a href="blog-details.html">How to appear in Google’s #1 page</a>
                                <p>Lorem ipsum dolor sit amet, consectetuer adipi scing elit, sed diam nonummy nibh euismod tincidunt ut laoreet.</p>
                            </div>
                        </div>
                        <!--end blog features box-->
                    </div>
                </div>
            </div>
            <!--read more blog button-->
            <div><a href="blog.html" class="btn btn-fill full-width">read more blog</a></div>
            <!--end read more blog button-->
        </div>
    </section>