 <!-- ++++ banner ++++ -->
    <section class="banner  o-hidden banner-inner seo-banner">
        <div class="container">
            <!--banner text-->
            <div class="banner-txt">
                <h1>Search Engine Optimization</h1>
                <p class="semi-bold">Driving more traffic to your website by increasing rank in different
                    <br /> search engines.</p>
                <div><a href="#process-tab" class="medium-btn2 btn btn-nofill page-scroll">OUR Process</a></div>
            </div>
            <!--end banner text-->
        </div>
    </section>
    <!-- ++++ end banner ++++ -->
    <!-- ++++ seo service description ++++ -->
    <section class="bg-white o-hidden common-form-section  service-description-section">
        <div class="container">
            <!--section title -->
            <h2 class="b-clor">Be the Number One</h2>
            <hr class="dark-line" />
            <!-- end section title -->
            <div class="row">
                <div class="col-lg-7 col-md-7 col-sm-6 col-xs-12">
                    <h3 class="semi-bold">Stand Out in the Crowd!</h3>
                    <p class="regular-text">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. </p>
                    <ul>
                        <li><span class="icon-clipboard-pencil"></span>Get free SEO audit.</li>
                        <li><span class="icon-find-replace"></span>Thorough optimization to increase rank.</li>
                        <li><span class="icon-laptop-phone"></span>Keywords and landing page integration.</li>
                    </ul>
                    <p class="regular-text">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna. Quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. </p>
                </div>
                <div class="col-lg-1 col-md-1 col-sm-1 col-xs-12"> </div>
                <div class="col-lg-4 col-md-4 col-sm-5 col-xs-12">
                    <div class="customise-form">
                        <form class="email_form" method="post">
                            <div class="form-element-wrapper">
                                <h3>Get a Free Quote</h3>
                                <div class="form-group customised-formgroup">
                                    <span class="icon-user"></span>
                                    <input type="text" name="full_name" class="form-control" placeholder="Name">
                                </div>
                                <div class="form-group customised-formgroup">
                                    <span class="icon-envelope"></span>
                                    <input type="email" name="email" class="form-control" placeholder="Email">
                                </div>
                                <div class="form-group customised-formgroup">
                                    <span class="icon-telephone"></span>
                                    <input type="text" name="phone" class="form-control" placeholder="Phone">
                                </div>
                                <div class="form-group customised-formgroup">
                                    <span class="icon-laptop"></span>
                                    <input type="text" name="website" class="form-control" placeholder="Website">
                                </div>
                                <div class="form-group customised-formgroup">
                                    <span class="icon-bubble"></span>
                                    <textarea name="message" class="form-control" placeholder="Message"></textarea>
                                </div>
                            </div>
                            <div>
                                <button type="submit" class="btn btn-fill full-width">GET A QUOTE<span class="icon-chevron-right"></span></button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- ++++ end seo service description ++++ -->
    <!-- ++++ design process section start ++++ -->
    <section class="bg-white design-process-section" id="process-tab">
        <div class="container">
            <!--section title -->
            <h2 class="b-clor text-align-center">Our SEO Process</h2>
            <p class="regular-text text-align-center">Our comprehensive SEO strategy ensures increase in ranking in different search engines.</p>
            <!--end section title -->
            <div class="row">
                <div class="col-xs-12">
                    <!-- design process steps-->
                    <!-- Nav tabs -->
                    <ul class="nav nav-tabs process-model more-icon-preocess" role="tablist">
                        <li role="presentation" class="active"><a href="#discover" aria-controls="discover" role="tab" data-toggle="tab"><span class="icon-magnifier"></span>
                                    <p>Discover</p>
                                </a></li>
                        <li role="presentation"><a href="#strategy" aria-controls="strategy" role="tab" data-toggle="tab"><span class="icon-archery"></span>
                                    <p>Strategy</p>
                                </a></li>
                        <li role="presentation"><a href="#optimization" aria-controls="optimization" role="tab" data-toggle="tab"><span class="icon-puzzle"></span>
                                    <p>Optimization</p>
                                </a></li>
                        <li role="presentation"><a href="#content" aria-controls="content" role="tab" data-toggle="tab"><span class="icon-papers"></span>
                                    <p>Content</p>
                                </a></li>
                        <li role="presentation"><a href="#reporting" aria-controls="reporting" role="tab" data-toggle="tab"><span class="icon-pie-chart"></span>
                                    <p>Reporting</p>
                                </a></li>
                    </ul>
                    <!-- end design process steps-->
                    <!-- Tab panes -->
                    <div class="tab-content">
                        <div role="tabpanel" class="tab-pane active" id="discover">
                            <div class="design-process-content">
                                <h3 class="semi-bold">Discovery</h3>
                                <p class="regular-text">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincid unt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullam corper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat.</p>
                                <p class="regular-text">Investigationes demonstraverunt lectores legere me lius quod ii legunt saepius. Claritas est etiam processus dynamicus, qui sequitur mutationem consuetudium lectorum. </p>
                                <img src="images/design-process-img.jpg" alt="seo process" class="img-responsive" /> </div>
                        </div>
                        <div role="tabpanel" class="tab-pane" id="strategy">
                            <div class="design-process-content">
                                <h3 class="semi-bold">Strategy</h3>
                                <p class="regular-text">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincid unt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullam corper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat.</p>
                                <p class="regular-text">Investigationes demonstraverunt lectores legere me lius quod ii legunt saepius. Claritas est etiam processus dynamicus, qui sequitur mutationem consuetudium lectorum.</p>
                                <img src="images/seo-discovery.jpg" alt="design process" class="img-responsive" /> </div>
                        </div>
                        <div role="tabpanel" class="tab-pane" id="optimization">
                            <div class="design-process-content">
                                <h3 class="semi-bold">Optimization</h3>
                                <p class="regular-text">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincid unt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullam corper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat.</p>
                                <p class="regular-text">Investigationes demonstraverunt lectores legere me lius quod ii legunt saepius. Claritas est etiam processus dynamicus, qui sequitur mutationem consuetudium lectorum. </p>
                                <img src="images/design-process-img.jpg" alt="seo process" class="img-responsive" /> </div>
                        </div>
                        <div role="tabpanel" class="tab-pane" id="content">
                            <div class="design-process-content">
                                <h3 class="semi-bold">Content</h3>
                                <p class="regular-text">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincid unt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullam corper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat.</p>
                                <p class="regular-text">Investigationes demonstraverunt lectores legere me lius quod ii legunt saepius. Claritas est etiam processus dynamicus, qui sequitur mutationem consuetudium lectorum. </p>
                                <img src="images/seo-discovery.jpg" alt="seo process" class="img-responsive" /> </div>
                        </div>
                        <div role="tabpanel" class="tab-pane" id="reporting">
                            <div class="design-process-content">
                                <h3 class="semi-bold">Reporting</h3>
                                <p class="regular-text">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincid unt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullam corper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat.</p>
                                <p class="regular-text">Investigationes demonstraverunt lectores legere me lius quod ii legunt saepius. Claritas est etiam processus dynamicus, qui sequitur mutationem consuetudium lectorum. </p>
                                <img src="images/design-process-img.jpg" alt="seo process" class="img-responsive" /> </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- ++++ end design process section ++++ -->
    <!-- ++++ featured design ++++ -->
    <section class="bg-white o-hidden long-box-wrapper">
        <div class="container">
            <!--section title -->
            <h2 class="b-clor">Case Studies</h2>
            <hr class="dark-line" />
            <!--end section title -->
            <div class="row">
                <div class="col-xs-6 col-sm-6">
                    <div class="box-content-with-img"> <img src="images/seo-case-studies-one.jpg" class="img-responsive" alt="seo-case-studies" />
                        <div class="box-content-text">
                            <h3 class="semi-bold">Client Name</h3>
                            <p class="regular-text">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.</p>
                            <ul>
                                <li>Developed brand positioning and WordPress website.</li>
                                <li>Planned sitemap and user flows for 2 distinct target audiences.</li>
                                <li>Created strategy and designs for desktop and mobile platforms.</li>
                            </ul>
                            <a href="case-studies-details.html" class="medium-btn2 btn btn-fill">VIEW DETAILS</a> </div>
                    </div>
                </div>
                <div class="col-xs-6 col-sm-6">
                    <div class="box-content-with-img"> <img src="images/seo-case-studies-two.jpg" class="img-responsive" alt="seo-case-studies" />
                        <div class="box-content-text">
                            <h3 class="semi-bold">Client Name</h3>
                            <p class="regular-text">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.</p>
                            <ul>
                                <li>Developed brand positioning and WordPress website.</li>
                                <li>Planned sitemap and user flows for 2 distinct target audiences.</li>
                                <li>Created strategy and designs for desktop and mobile platforms.</li>
                            </ul>
                            <a href="case-studies-details.html" class="medium-btn2 btn btn-fill">VIEW DETAILS</a> </div>
                    </div>
                </div>
                <div class="col-xs-12">
                    <div><a href="case-studies.html" class="btn btn-fill full-width">Discover more</a></div>
                </div>
            </div>
        </div>
    </section>
    <!-- ++++ end featured design ++++ -->
    <!-- ++++ footer ++++ -->