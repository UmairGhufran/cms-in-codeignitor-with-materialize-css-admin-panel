<section class="banner  o-hidden banner-inner logo-branding-banner">
        <div class="container">
            <!--banner text-->
            <div class="banner-txt">
                <h1>Logo &amp; Branding</h1>
                <p class="semi-bold">Give your business a unique logo to stand out from the crowd.
                    <br /> We’ll create custom tailored logo specifically for your company.</p>
                <a href="#process-tab" class="medium-btn2 btn btn-nofill page-scroll">OUR Process</a> </div>
            <!--end banner text-->
        </div>
    </section>
    <!-- ++++ end banner ++++ -->
    <!-- ++++ actual advertisement ++++ -->
    <section class="bg-white o-hidden common-form-section  service-description-section">
        <div class="container">
            <!--section title -->
            <h2 class="b-clor">Why Branding Matters?</h2>
            <hr class="dark-line" />
            <!--end section title -->
            <div class="row">
                <div class="col-lg-7 col-md-7 col-sm-6 col-xs-12">
                    <h3 class="semi-bold">Stand Out in the Crowd!</h3>
                    <p class="regular-text">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. </p>
                    <ul>
                        <li><span class="icon-leaf"></span>Modern and evergreen logo for your business.</li>
                        <li><span class="icon-users"></span>Branding that stands out in the crowd.</li>
                        <li><span class="icon-road-sign"></span>Strategic approach towards redesigning brand.</li>
                    </ul>
                    <p class="regular-text">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna. Quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. </p>
                </div>
                <div class="col-lg-1 col-md-1 col-sm-1 col-xs-12"> </div>
                <div class="col-lg-4 col-md-4 col-sm-5 col-xs-12">
                    <div class="customise-form">
                        <form class="email_form" method="post">
                            <div class="form-element-wrapper">
                                <h3>Get a Free Quote</h3>
                                <div class="form-group customised-formgroup">
                                    <span class="icon-user"></span>
                                    <input type="text" name="full_name" class="form-control" placeholder="Name">
                                </div>
                                <div class="form-group customised-formgroup">
                                    <span class="icon-envelope"></span>
                                    <input type="email" name="email" class="form-control" placeholder="Email">
                                </div>
                                <div class="form-group customised-formgroup">
                                    <span class="icon-telephone"></span>
                                    <input type="text" name="phone" class="form-control" placeholder="Phone">
                                </div>
                                <div class="form-group customised-formgroup">
                                    <span class="icon-laptop"></span>
                                    <input type="text" name="website" class="form-control" placeholder="Website">
                                </div>
                                <div class="form-group customised-formgroup">
                                    <span class="icon-bubble"></span>
                                    <textarea name="message" class="form-control" placeholder="Message"></textarea>
                                </div>
                            </div>
                            <div>
                                <button type="submit" class="btn btn-fill full-width">GET A QUOTE<span class="icon-chevron-right"></span></button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- ++++ end actual advertisement ++++ -->
    <!-- ++++ process section start ++++ -->
    <section class="bg-white design-process-section" id="process-tab">
        <div class="container">
            <!--section title -->
            <h2 class="b-clor text-align-center">Our Logo Design Process</h2>
            <p class="regular-text text-align-center">Our comprehensive logo design strategy ensures a perfectly crafted logo for your business.</p>
            <!--end section title -->
            <div class="row">
                <div class="col-xs-12">
                    <!-- design process steps-->
                    <!-- Nav tabs -->
                    <ul class="nav nav-tabs process-model more-icon-preocess" role="tablist">
                        <li role="presentation" class="active"><a href="#exploration" aria-controls="exploration" role="tab" data-toggle="tab"><span class="icon-magnifier"></span>
                                    <p>Exploration</p>
                                </a></li>
                        <li role="presentation"><a href="#sketches" aria-controls="sketches" role="tab" data-toggle="tab"><span class="icon-pencil3"></span>
                                    <p>Sketches</p>
                                </a></li>
                        <li role="presentation"><a href="#digitization" aria-controls="digitization" role="tab" data-toggle="tab"><span class="icon-vector"></span>
                                    <p>Digitization</p>
                                </a></li>
                        <li role="presentation"><a href="#color" aria-controls="color" role="tab" data-toggle="tab"><span class=" icon-palette"></span>
                                    <p>Color</p>
                                </a></li>
                        <li role="presentation"><a href="#logo" aria-controls="logo" role="tab" data-toggle="tab"><span class="icon-star"></span>
                                    <p>Final Logo</p>
                                </a></li>
                    </ul>
                    <!-- end design process steps-->
                    <!-- Tab panes -->
                    <div class="tab-content">
                        <div role="tabpanel" class="tab-pane active" id="exploration">
                            <div class="design-process-content">
                                <h3 class="semi-bold">Exploration</h3>
                                <p class="regular-text">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincid unt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullam corper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat.</p>
                                <p class="regular-text">Investigationes demonstraverunt lectores legere me lius quod ii legunt saepius. Claritas est etiam processus dynamicus, qui sequitur mutationem consuetudium lectorum. </p>
                                <img src="images/design-process-img.jpg" alt="seo process" class="img-responsive" /> </div>
                        </div>
                        <div role="tabpanel" class="tab-pane" id="sketches">
                            <div class="design-process-content">
                                <h3 class="semi-bold">Sketches</h3>
                                <p class="regular-text">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincid unt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullam corper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat.</p>
                                <p class="regular-text">Investigationes demonstraverunt lectores legere me lius quod ii legunt saepius. Claritas est etiam processus dynamicus, qui sequitur mutationem consuetudium lectorum.</p>
                                <img src="images/seo-discovery.jpg" alt="design process" class="img-responsive" /> </div>
                        </div>
                        <div role="tabpanel" class="tab-pane" id="digitization">
                            <div class="design-process-content">
                                <h3 class="semi-bold">Digitization</h3>
                                <p class="regular-text">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincid unt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullam corper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat.</p>
                                <p class="regular-text">Investigationes demonstraverunt lectores legere me lius quod ii legunt saepius. Claritas est etiam processus dynamicus, qui sequitur mutationem consuetudium lectorum. </p>
                                <img src="images/design-process-img.jpg" alt="seo process" class="img-responsive" /> </div>
                        </div>
                        <div role="tabpanel" class="tab-pane" id="color">
                            <div class="design-process-content">
                                <h3 class="semi-bold">Color</h3>
                                <p class="regular-text">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincid unt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullam corper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat.</p>
                                <p class="regular-text">Investigationes demonstraverunt lectores legere me lius quod ii legunt saepius. Claritas est etiam processus dynamicus, qui sequitur mutationem consuetudium lectorum. </p>
                                <img src="images/seo-discovery.jpg" alt="seo process" class="img-responsive" /> </div>
                        </div>
                        <div role="tabpanel" class="tab-pane" id="logo">
                            <div class="design-process-content">
                                <h3 class="semi-bold">Final Logo</h3>
                                <p class="regular-text">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincid unt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullam corper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat.</p>
                                <p class="regular-text">Investigationes demonstraverunt lectores legere me lius quod ii legunt saepius. Claritas est etiam processus dynamicus, qui sequitur mutationem consuetudium lectorum. </p>
                                <img src="images/design-process-img.jpg" alt="seo process" class="img-responsive" /> </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--end process section-->
    <!-- ++++ featured design ++++ -->
    <section class="o-hidden bg-white featured-design-section mobile-app-featured">
        <div class="container">
            <!--section title -->
            <h2 class="b-clor">Featured Designs</h2>
            <hr class="dark-line" />
            <!--end section title -->
            <div class="row margin-top-40">
                <div class="col-xs-6 col-sm-4">
                    <div class="featured-img-wrapper"> <img src="images/logo-branding-one.jpg" class="img-responsive" alt="Featured image" />
                        <div class="por-overley">
                            <div class="text-inner"> <a class="btn btn-nofill proDetModal">DISCOVER</a> </div>
                        </div>
                    </div>
                </div>
                <div class="col-xs-6 col-sm-4">
                    <div class="featured-img-wrapper"> <img src="images/logo-branding-two.jpg" class="img-responsive" alt="Featured image" />
                        <div class="por-overley">
                            <div class="text-inner"> <a class="btn btn-nofill proDetModal">DISCOVER</a> </div>
                        </div>
                    </div>
                </div>
                <div class="col-xs-6 col-sm-4">
                    <div class="featured-img-wrapper"> <img src="images/logo-branding-three.jpg" class="img-responsive" alt="Featured image" />
                        <div class="por-overley">
                            <div class="text-inner"> <a class="btn btn-nofill proDetModal">DISCOVER</a> </div>
                        </div>
                    </div>
                </div>
                <div class="col-xs-6 col-sm-4">
                    <div class="featured-img-wrapper"> <img src="images/portfolio/portfolio-six.jpg" class="img-responsive" alt="Featured image" />
                        <div class="por-overley">
                            <div class="text-inner"> <a class="btn btn-nofill proDetModal">DISCOVER</a> </div>
                        </div>
                    </div>
                </div>
                <div class="col-xs-6 col-sm-4">
                    <div class="featured-img-wrapper"> <img src="images/portfolio/por-fea-project.jpg" class="img-responsive" alt="Featured image" />
                        <div class="por-overley">
                            <div class="text-inner"> <a class="btn btn-nofill proDetModal">DISCOVER</a> </div>
                        </div>
                    </div>
                </div>
                <div class="col-xs-6 col-sm-4">
                    <div class="featured-img-wrapper"> <img src="images/portfolio/portfolio-seven.jpg" class="img-responsive" alt="Featured image" />
                        <div class="por-overley">
                            <div class="text-inner"> <a class="btn btn-nofill proDetModal">DISCOVER</a> </div>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12"> <a href="portfolio.html" class="btn btn-fill full-width">Discover more</a> </div>
            </div>
        </div>
    </section>
    <!-- ++++ end featured design ++++ -->