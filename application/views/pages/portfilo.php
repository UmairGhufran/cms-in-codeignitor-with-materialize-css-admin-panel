    
       <section class="banner  o-hidden banner-inner portfolio-banner">
        <div class="container">
            <!--banner text-->
            <div class="banner-txt">
                <h1>Portfolio</h1>
                <p class="semi-bold">We use strategic approaches to provide our clients with high-end.
                    <br /> services that ensure superior customer satisfaction.</p>
                <a href="#more-portfolio" class="medium-btn2 btn btn-nofill page-scroll">DISCOVER MORE</a> </div>
            <!--end banner text-->
        </div>
    </section>
    <!-- ++++ end banner ++++ -->
    <!-- ++++ Featured Project content ++++ -->
    <section class="bg-white o-hidden portflio-content">
        <div class="container">
            <!--section title -->
            <h2 class="b-clor">Featured Projects</h2>
            <hr class="dark-line" />
            <!--end section title -->
            <div class="row">
                <!--featured project content box-->
                <div class="col-xs-12 col-sm-4">
                    <div class="box-content-with-img"> <img src="<?php echo base_url();?>assets/Front/images/portfolio/por-fea-project.jpg" class="img-responsive" alt="seo-case-studies" />
                        <div class="box-content-text">
                            <p class="gray-text">Featured - Design</p>
                            <h3 class="semi-bold"><a class="proDetModal">Logo Design</a></h3>
                            <p class="regular-text">Lorem ipsum dolor sit amet, consect etuer adipi scing elit, sed diam nonum my nibh euismod tincidunt.</p>
                        </div>
                        <img src="<?php echo base_url();?>assets/Front/images/featured.png" alt="featured tag" class="featured-tag" /> </div>
                </div>
                <!--end featured project content box-->
                <!--featured project content box-->
                <div class="col-xs-12 col-sm-4">
                    <div class="box-content-with-img"> <img src="<?php echo base_url();?>assets/Front/images/portfolio/portfolio-three.jpg" class="img-responsive" alt="seo-case-studies" />
                        <div class="box-content-text">
                            <p class="gray-text">Featured - Development</p>
                            <h3 class="semi-bold"><a class="proDetModal">Restaurant Website</a></h3>
                            <p class="regular-text">Lorem ipsum dolor sit amet, consect etuer adipi scing elit, sed diam nonum my nibh euismod tincidunt.</p>
                        </div>
                        <img src="<?php echo base_url();?>assets/Front/images/featured.png" alt="featured tag" class="featured-tag" /> </div>
                </div>
                <!--end featured project content box-->
                <!--featured project content box-->
                <div class="col-xs-12 col-sm-4">
                    <div class="box-content-with-img"> <img src="<?php echo base_url();?>assets/Front/images/portfolio/por-fea-project-three.jpg" class="img-responsive" alt="seo-case-studies" />
                        <div class="box-content-text">
                            <p class="gray-text">Featured - Online Marketing</p>
                            <h3 class="semi-bold"><a class="proDetModal">Facebook Cover Design</a></h3>
                            <p class="regular-text">Lorem ipsum dolor sit amet, consect etuer adipi scing elit, sed diam nonum my nibh euismod tincidunt.</p>
                        </div>
                        <img src="images/featured.png" alt="featured tag" class="featured-tag" /> </div>
                </div>
                <!--end featured project content box-->
            </div>
        </div>
    </section>
    <!-- ++++ end Featured Project content ++++ -->
    <!-- ++++ full portfolio section ++++ -->
    <section class="bg-white o-hidden product-section" id="more-portfolio">
        <div class="container">
            <!--section title -->
            <h2 class="b-clor">Full Portfolio</h2>
            <hr class="dark-line" />
            <!--end section title -->
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="pro-controls">
                        <button class="filter" data-filter="all">All Work</button>
                        <button class="filter" data-filter=".category-1">Website</button>
                        <button class="filter" data-filter=".category-2">Logo</button>
                        <button class="filter" data-filter=".category-3"> Mobile Apps</button>
                        <button class="filter" data-filter=".category-4">Social Media</button>
                    </div>
                </div>
            </div>
            <div id="portfolio-cat" class="port-cat-con">
                <div class="row">
                    <div class="mix category-1 col-md-4 col-sm-4 col-xs-6">
                        <div class="pro-item-img" style="background: url('<?php echo base_url();?>assets/Front/images/portfolio/portfolio-one.jpg')">
                            <div class="por-overley">
                                <div class="text-inner"> <a class="btn btn-nofill proDetModal" data-toggle="modal">DISCOVER</a> </div>
                            </div>
                        </div>
                    </div>
                    <div class="mix category-1 col-md-4 col-sm-4 col-xs-6">
                        <div class="pro-item-img" style="background: url('<?php echo base_url();?>assets/Front/images/portfolio/portfolio-two.jpg')">
                            <div class="por-overley">
                                <div class="text-inner"> <a class="btn btn-nofill proDetModal">DISCOVER</a> </div>
                            </div>
                        </div>
                    </div>
                    <div class="mix category-2 col-md-4 col-sm-4 col-xs-6">
                        <div class="pro-item-img" style="background: url('<?php echo base_url();?>assets/Front/images/portfolio/portfolio-three.jpg')">
                            <div class="por-overley">
                                <div class="text-inner"> <a class="btn btn-nofill proDetModal">DISCOVER</a> </div>
                            </div>
                        </div>
                    </div>
                    <div class="mix category-2 col-md-4 col-sm-4 col-xs-6">
                        <div class="pro-item-img" style="background: url('<?php echo base_url();?>assets/Front/images/portfolio/portfolio-four.jpg')">
                            <div class="por-overley">
                                <div class="text-inner"> <a class="btn btn-nofill proDetModal">DISCOVER</a> </div>
                            </div>
                        </div>
                    </div>
                    <div class="mix category-1 col-md-4 col-sm-4 col-xs-6">
                        <div class="pro-item-img" style="background: url('<?php echo base_url();?>assets/Front/images/portfolio/portfolio-five.jpg')">
                            <div class="por-overley">
                                <div class="text-inner"> <a class="btn btn-nofill proDetModal">DISCOVER</a> </div>
                            </div>
                        </div>
                    </div>
                    <div class="mix category-3 col-md-4 col-sm-4 col-xs-6">
                        <div class="pro-item-img" style="background: url('<?php echo base_url();?>assets/Front/images/portfolio/portfolio-six.jpg')">
                            <div class="por-overley">
                                <div class="text-inner"> <a class="btn btn-nofill proDetModal">DISCOVER</a> </div>
                            </div>
                        </div>
                    </div>
                    <div class="mix category-3 col-md-4 col-sm-4 col-xs-6">
                        <div class="pro-item-img" style="background: url('<?php echo base_url();?>assets/Front/images/portfolio/portfolio-seven.jpg')">
                            <div class="por-overley">
                                <div class="text-inner"> <a class="btn btn-nofill proDetModal">DISCOVER</a> </div>
                            </div>
                        </div>
                    </div>
                    <div class="mix category-4 col-md-4 col-sm-4 col-xs-6">
                        <div class="pro-item-img" style="background: url('<?php echo base_url();?>assets/Front/images/portfolio/por-fea-project.jpg')">
                            <div class="por-overley">
                                <div class="text-inner"> <a class="btn btn-nofill proDetModal">DISCOVER</a> </div>
                            </div>
                        </div>
                    </div>
                    <div class="mix category-4 col-md-4 col-sm-4 col-xs-6">
                        <div class="pro-item-img" style="background: url('<?php echo base_url();?>assets/Front/images/portfolio/portfolio-three.jpg')">
                            <div class="por-overley">
                                <div class="text-inner"> <a class="btn btn-nofill proDetModal">DISCOVER</a> </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xs-12"> <a href="#" class="btn btn-fill full-width">Discover more</a> </div>
    </section>
    <!--  ++++ end full portfolio section ++++ -->
    <!-- ++++ footer ++++ -->