<!-- ++++ banner ++++ -->
    <section class="banner  o-hidden banner-inner website-banner">
        <div class="container">
            <!--banner text-->
            <div class="banner-txt">
                <h1>Website Design</h1>
                <p class="semi-bold">Great websites add great values to your business. From
                    <br /> wire-framing to PSD designing, we do it all.</p>
                <a href="#process-tab" class="medium-btn3 btn btn-nofill page-scroll">OUR Process</a> </div>
            <!--end banner text-->
        </div>
    </section>
    <!-- ++++ end banner ++++ -->
    <!-- ++++ responsive web design ++++ -->
    <section class="bg-white o-hidden common-form-section  service-description-section">
        <div class="container">
            <!--section title -->
            <h2 class="b-clor">Responsive Web Design</h2>
            <hr class="dark-line" />
            <!--end section title -->
            <div class="row">
                <div class="col-lg-7 col-md-7 col-sm-6 col-xs-12">
                    <h3 class="semi-bold">One Design for All Platforms</h3>
                    <p class="regular-text">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. </p>
                    <ul>
                        <li><span class="icon-laptop-phone"></span>One design for your desktop, tab and mobile.</li>
                        <li><span class="icon-lollipop"></span>Beautiful and modern design that makes difference.</li>
                        <li><span class="icon-rocket"></span>Boost your sales with strategically built user experience.</li>
                    </ul>
                    <p class="regular-text">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna. Quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. </p>
                </div>
                <div class="col-lg-1 col-md-1 col-sm-1 col-xs-12"> </div>
                <div class="col-lg-4 col-md-4 col-sm-5 col-xs-12">
                    <div class="customise-form">
                        <form class="email_form" method="post">
                            <div class="form-element-wrapper">
                                <h3>Get a Free Quote</h3>
                                <div class="form-group customised-formgroup">
                                    <span class="icon-user"></span>
                                    <input type="text" name="full_name" class="form-control" placeholder="Name">
                                </div>
                                <div class="form-group customised-formgroup">
                                    <span class="icon-envelope"></span>
                                    <input type="email" name="email" class="form-control" placeholder="Email">
                                </div>
                                <div class="form-group customised-formgroup">
                                    <span class="icon-telephone"></span>
                                    <input type="text" name="phone" class="form-control" placeholder="Phone">
                                </div>
                                <div class="form-group customised-formgroup">
                                    <span class="icon-laptop"></span>
                                    <input type="text" name="website" class="form-control" placeholder="Website">
                                </div>
                                <div class="form-group customised-formgroup">
                                    <span class="icon-bubble"></span>
                                    <textarea name="message" class="form-control" placeholder="Message"></textarea>
                                </div>
                            </div>
                            <div>
                                <button type="submit" class="btn btn-fill full-width">GET A QUOTE<span class="icon-chevron-right"></span></button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- ++++ end responsive web design ++++ -->
    <!-- ++++ design process section start ++++ -->
    <section class=" bg-white design-process-section" id="process-tab">
        <div class="container">
            <!--section title -->
            <h2 class="b-clor text-align-center">Our Website Design Process</h2>
            <p class="regular-text text-align-center">Our comprehensive website design strategy ensures a perfectly crafted website for your business.</p>
            <!--end section title -->
            <div class="row">
                <div class="col-xs-12">
                    <!-- design process steps-->
                    <!-- Nav tabs -->
                    <ul class="nav nav-tabs process-model" role="tablist">
                        <li role="presentation" class="active">
                            <a href="#consultation" aria-controls="consultation" role="tab" data-toggle="tab"> <span class="icon-bubble-user"></span>
                                <p>Consultation</p>
                            </a>
                        </li>
                        <li role="presentation"><a href="#wire-frame" aria-controls="wire-frame" role="tab" data-toggle="tab"><span class="icon-pencil-ruler"></span>
                                    <p>Wire-frame</p>
                                </a></li>
                        <li role="presentation"><a href="#final-design" aria-controls="final-design" role="tab" data-toggle="tab"><span class="icon-laptop-phone"></span>
                                    <p>Final Design</p>
                                </a></li>
                    </ul>
                    <!-- end design process steps-->
                    <!-- Tab panes -->
                    <div class="tab-content">
                        <div role="tabpanel" class="tab-pane active" id="consultation">
                            <div class="design-process-content">
                                <h3 class="semi-bold">Consultation</h3>
                                <p class="regular-text">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincid unt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullam corper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat.</p>
                                <p class="regular-text">Investigationes demonstraverunt lectores legere me lius quod ii legunt saepius. Claritas est etiam processus dynamicus, qui sequitur mutationem consuetudium lectorum.</p>
                                <img src="images/design-process-img.jpg" alt="design process" class="img-responsive" /> </div>
                        </div>
                        <div role="tabpanel" class="tab-pane" id="wire-frame">
                            <div class="design-process-content">
                                <h3 class="semi-bold">Wire-frame</h3>
                                <p class="regular-text">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincid unt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullam corper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat.</p>
                                <p class="regular-text">Investigationes demonstraverunt lectores legere me lius quod ii legunt saepius. Claritas est etiam processus dynamicus, qui sequitur mutationem consuetudium lectorum.</p>
                                <img src="images/seo-discovery.jpg" alt="design process" class="img-responsive" /> </div>
                        </div>
                        <div role="tabpanel" class="tab-pane" id="final-design">
                            <div class="design-process-content">
                                <h3 class="semi-bold">Final Design</h3>
                                <p class="regular-text">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincid unt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullam corper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat.</p>
                                <p class="regular-text">Investigationes demonstraverunt lectores legere me lius quod ii legunt saepius. Claritas est etiam processus dynamicus, qui sequitur mutationem consuetudium lectorum.</p>
                                <img src="images/design-process-img.jpg" alt="design process" class="img-responsive" /> </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- ++++ end design process section ++++ -->
    <!-- ++++ featured design ++++ -->
    <section class="o-hidden bg-white featured-design-section mobile-app-featured">
        <div class="container">
            <!--section title -->
            <h2 class="b-clor">Featured Designs</h2>
            <hr class="dark-line" />
            <!--end section title -->
            <div class="row margin-top-40">
                <div class="col-xs-6 col-sm-4">
                    <div class="featured-img-wrapper"> <img src="images/web-design-fe-img-1.jpg" class="img-responsive" alt="Featured image" />
                        <div class="por-overley">
                            <div class="text-inner"> <a class="btn btn-nofill proDetModal">DISCOVER</a> </div>
                        </div>
                    </div>
                </div>
                <div class="col-xs-6 col-sm-4">
                    <div class="featured-img-wrapper"> <img src="images/web-design-fe-img-2.jpg" class="img-responsive" alt="Featured image" />
                        <div class="por-overley">
                            <div class="text-inner"> <a class="btn btn-nofill proDetModal">DISCOVER</a> </div>
                        </div>
                    </div>
                </div>
                <div class="col-xs-6 col-sm-4">
                    <div class="featured-img-wrapper"> <img src="images/web-design-fe-img-3.jpg" class="img-responsive" alt="Featured image" />
                        <div class="por-overley">
                            <div class="text-inner"> <a class="btn btn-nofill proDetModal">DISCOVER</a> </div>
                        </div>
                    </div>
                </div>
                <div class="col-xs-6 col-sm-4">
                    <div class="featured-img-wrapper"> <img src="images/web-design-fe-img-4.jpg" class="img-responsive" alt="Featured image" />
                        <div class="por-overley">
                            <div class="text-inner"> <a class="btn btn-nofill proDetModal">DISCOVER</a> </div>
                        </div>
                    </div>
                </div>
                <div class="col-xs-6 col-sm-4">
                    <div class="featured-img-wrapper"> <img src="images/web-design-fe-img-5.jpg" class="img-responsive" alt="Featured image" />
                        <div class="por-overley">
                            <div class="text-inner"> <a class="btn btn-nofill proDetModal">DISCOVER</a> </div>
                        </div>
                    </div>
                </div>
                <div class="col-xs-6 col-sm-4">
                    <div class="featured-img-wrapper"> <img src="images/web-design-fe-img-6.jpg" class="img-responsive" alt="Featured image" />
                        <div class="por-overley">
                            <div class="text-inner"> <a class="btn btn-nofill proDetModal">DISCOVER</a> </div>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12"> <a href="portfolio.html" class="btn btn-fill full-width">Discover more</a> </div>
            </div>
        </div>
    </section>
    <!-- ++++ end featured design ++++ -->
    <!-- ++++ footer ++++ -->